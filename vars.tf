# Configure these variables

variable "resource-group" {
    default = "default"
}

variable "service-type" {
    default = "cloud-object-storage"
}

variable "service-plan" {
    default = "standard"
}

variable "service-name" {
    default = "COS_object"
}

variable "location" {
    default = "global"
}

variable "service-key-name" {
    default = "COS_object"
}

variable "service-key-role" {
    default = "Writer"
}