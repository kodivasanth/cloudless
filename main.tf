# Create an arbitrary local resource
data "ibm_resource_group" "group" {
    name = "${var.resource-group}"
}
resource "ibm_resource_instance" "CloudObjectStorage" {
name = "${var.service-name}"
service = "${var.service-type}"
plan = "${var.service-plan}"
resource_group_id = "${data.ibm_resource_group.group.id}"
location = "${var.location}"
}

resource "ibm_resource_key" "CloudObjectStorage_Resource_Key" {
name = "${var.service-key-name}"
role = "${var.service-key-role}"
resource_instance_id = "${ibm_resource_instance.CloudObjectStorage.id}"
}
provider "ibm" {
 }
